call conda create -n temp_env python==3.10 -y
call conda activate temp_env
call pip install -r requirements.txt
call pip install auto-py-to-exe
call cd ..
call pyinstaller --noconfirm --onefile --console --add-data "src/lib;lib/" src/action_runner.py 
call rmdir /s /q build
call del /s /q action_runner.spec
call conda deactivate
call conda remove --name temp_env --all -y