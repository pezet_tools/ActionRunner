import logging

logger = logging.getLogger('ActionRunnerLogger')
loggerHandler = logging.StreamHandler()
loggerHandler.setLevel(logging.INFO)
loggerHandler.setFormatter(logging.Formatter('[%(asctime)s] - %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S'))
logger.addHandler(loggerHandler)

elogger = logging.getLogger('ActionRunnerErrorLogger')
eloggerHandler = logging.StreamHandler()
eloggerHandler.setLevel(logging.INFO)
eloggerHandler.setFormatter(logging.Formatter('[%(asctime)s] - %(levelname)s %(message)s ** Check for verbose logs (run app with -v argument)', datefmt='%Y-%m-%d %H:%M:%S'))
elogger.addHandler(eloggerHandler)
