import os
import time
from pathlib import Path
from mcrypt import Mcrypt
from lib.signs import sign_dict
from lib.logger import logger, elogger
from pynput.keyboard import Controller


class ActionBuilder():

    actions = []

    @staticmethod
    def build_action(action: dict):
        _type = action.get("type")
        if _type == "app":
            ActionBuilder.actions.append(AppAction(action.get("name"), action.get("path")))
        elif _type == "sleep":
            ActionBuilder.actions.append(SleepAction(action.get("name"), action.get("value")))
        elif _type == "keystroke":
            ActionBuilder.actions.append(KeystrokeAction(action.get("name"), action.get("sequence")))
        else:
            elogger.error("Unknown action type! Use [app, sleep, keystroke]")
            ActionBuilder.actions.append(None)
        

class Action:

    action_id = 0

    def __init__(self, name: str):
        self.id = Action.set_action_id()
        self.name = name
        self.is_ok = False
        logger.debug(f"Action [{self.id}] type={self.__class__.__name__}")

    @staticmethod
    def set_action_id():
        Action.action_id += 1
        return Action.action_id
        
    def run_action(self):
        logger.debug(f"[Action {self.id}] - Started")

    def check_action(self):
        pass


class AppAction(Action):

    def __init__(self, name: str, path: str):
        super().__init__(name)
        self.path = path
        logger.debug(f"AppPath={self.path}")
        self.check_action()
        logger.debug(f"IsOk={self.is_ok}")
    
    def run_action(self):
        super().run_action()
        logger.info(f"Run app: {self.path}")
        os.system(f"start {self.path}")
    
    def check_action(self):
        _file = MyPath(self.path)
        if _file.is_file():
            self.is_ok = True
        else:
            elogger.error(f"{self.path}")
            elogger.error(f"AppPath does not exist! Rememeber to enclose strings with quotes!")


class SleepAction(Action):

    def __init__(self, name, value: int):
        super().__init__(name)
        self.value = value
        logger.debug(f"SleepValue={self.value}")
        self.check_action()
        logger.debug(f"IsOk={self.is_ok}")
    
    def run_action(self):
        super().run_action()
        logger.info(f"Sleep for: {self.value} seconds")
        time.sleep(self.value)

    def check_action(self):
        if (type(self.value)) == int:
            self.is_ok = True
        else:
            elogger.error(f"SleepValue is not an integer!")


class KeystrokeAction(Action):

    MODE_PARALLEL = 'parallel'
    MODE_SERIES = 'series'
    MODE_PASSWORD = 'password'
    MODES = [MODE_PARALLEL, MODE_SERIES, MODE_PASSWORD]

    def __init__(self, name, sequence: list):
        super().__init__(name)     
        self.sequence = sequence
        self.keyboard = Controller()    
        for seq in self.sequence:
            logger.debug(f"Sequence={seq}")
        self.check_action()
        logger.debug(f"IsOk={self.is_ok}")

    def check_sign(self, value: str) -> str:
        return sign_dict.get(value, value)

    def press_in_sequence(self, series: list):
        for element in series:
            self.write_element(element)

    def press_passwords_in_sequence(self, series: list):
        mc = Mcrypt('.key')
        for element in series:
            self.write_element(mc.decrypt(element))

    def press_in_parallel(self, series: list):
        if len(series) > 1:
            with self.keyboard.pressed(sign_dict.get(series[0])):
                self.press_in_parallel(series[1:])
        else:
            self.keyboard.press(sign_dict.get(series[0], series[0]))
            self.keyboard.release(sign_dict.get(series[0], series[0]))

    def write_element(self, element: str):
        if element in sign_dict:
            self.keyboard.press(sign_dict.get(element))
            self.keyboard.release(sign_dict.get(element))
        else:
            self.keyboard.type(element)


    def check_action(self):
        error_types = []
        for seq in self.sequence:
            if seq.get('mode') not in KeystrokeAction.MODES:
                error_types.append('wrong_type')
            
            _value = seq.get('value')
            if type(_value) != list:
                error_types.append('value_not_a_list')

            for _v in _value:
                if type(_v) != str:
                    error_types.append('value_element_not_a_string')

            if seq.get('mode') == "password":
                try:
                    mc = Mcrypt('.key')
                except FileNotFoundError as err:
                    error_types.append('secret_key_for_password_decryption_does_not_exist')
        
        if not error_types:
            self.is_ok = True
        else:
            elogger.error(f"ErrorsList: {set(error_types)}")


    def run_action(self):
        super().run_action()
        for seq in self.sequence:
            _mode = seq.get('mode')
            _value = seq.get('value')
            logger.info(f"Push keys: {_value}")
            logger.debug(f"Push mode={_mode}")
            if _mode == "series":
                self.press_in_sequence(_value)
            elif _mode == "password":
                self.press_passwords_in_sequence(_value)
            elif _mode == "parallel":
                self.press_in_parallel(_value)


class MyPath():

    def __init__(self, _path: str):
        self._path = _path
        self.nomralize()

    def nomralize(self):
        _indx= self.find_space_no_quotes(self._path)
        if _indx > 0:
            _temp = r'{}'.format(self._path[:_indx])
        else:
            _temp = r'{}'.format(self._path)
        _temp = _temp.replace('"', '').replace(':\\', ':\\\\')
        self._pathlib_path = Path(_temp)

    def is_file(self):
        return self._pathlib_path.is_file()
        
    def find_space_no_quotes(self, _path: str):
        is_quote = False
        for indx_, char_ in enumerate(_path):
            if char_ == '"' and not is_quote:
                is_quote = True
            elif char_ == '"' and is_quote:
                is_quote = False
            elif char_ == ' ' and not is_quote:
                return indx_
        return -1