from sys import argv
import yaml
import time
from logging import INFO, DEBUG
from lib.actions import ActionBuilder
from lib.logger import logger


class ActionRunner():

    def __init__(self, configuration: str, verbose: bool = False):
        logger.setLevel(DEBUG) if verbose else logger.setLevel(INFO)
        for _handler in logger.handlers:
            _handler.setLevel(DEBUG) if verbose else _handler.setLevel(INFO)

        logger.info(f"Set logging level - {self.get_logging_level()}")
        if logger.level == INFO:
            logger.info("For more logs run app with -v parameter.")
        self.configuration = self.read_config(configuration)
        self.actions = list()

    def get_logging_level(self):
        _level = logger.level
        if _level == DEBUG: return "DEBUG"
        if _level == INFO: return "INFO"

    def read_config(self, filename: str) -> dict:
        logger.debug(f"Reading configuration from {filename}...")
        with open(filename, 'r') as config:
            return yaml.safe_load(config)

    def build_action_list_sequence(self):
        logger.debug("Building action list...")
        for action in self.configuration.get('actions'):
            logger.debug(f"{action=}")
            ActionBuilder.build_action(action)

    def check_actions(self):
        for action in ActionBuilder.actions:
            if not action or action.is_ok == False:
                logger.error(f"Found errors. Aplication Terminated.")
                exit(1)

    def run_actions(self):
        logger.info(f"Running {len(ActionBuilder.actions)} actions...")
        for action in ActionBuilder.actions:
            action.run_action()
        logger.info("All actions are finished.")
        time.sleep(10)

    def run(self):
        self.build_action_list_sequence()
        self.check_actions()
        self.run_actions()
        

if __name__ == "__main__":
    verbose = False
    if len(argv) > 1 and argv[1] == "-v":
        verbose = True
    apprunner = ActionRunner('config.yml', verbose)
    apprunner.run()
