Application for automatically running certain actions:

- Running applications
- Doing timeout
- Pressing keys (i.e. creating notes, inserting, credentials, etc)

Example template:

```yaml
actions: 
  - type: "app"
    name: "Notepad++"
    path: 'C:\"Program Files"\Notepad++\notepad++.exe'

  - type: "sleep"
    value: 3

  - type: "keystroke"
    sequence:
      - mode: "series"
        value: ["Hello!"]
      - mode: "parallel"
        value: ["ctrl", "n"]
      - mode: "password"
        value: ["gAAAAABjXZssPc-RpDM-axyaPCHvDgiR79g7BPkYoHmQQUkPeCWV5llwUu9ZDkD43VsqkmYETM3sxDdmpboJRCScwkZhF6lbww=="]
```

Parameters:

| action type | paramter | description |
| --- | --- | --- |
| app | [name] of the application<br> [path] to executable | Runs defined application |
| sleep | [value] time in seconds | Waits for specific amount of time |
| keystroke | [mode] <br> - [series] Runs defined list of strings in series<br> - [password] Runs in series defined list of encoded strings. Decodes them before typing<br> - [parallel] Runs defined list of strings in parallel | Presses keys in defined order based on mode type |

If user wants to use encoded type of strings, one must create a .key file with generated key. This key can be created by hand or generated using built-in mcrypt module from within python console.
```python
Mcrypt.generate_secret_key()
```
File '.key' will be created in the working directory with generated string key.
Then user needs to encode a string with mcrypt library using python console
```python
mc = Mcrypt('.key')
mc.encrypt('Your secret string')
```
Generated secret string needs to be written as a value inside a keystroke action type ['password'] as in the example above.
